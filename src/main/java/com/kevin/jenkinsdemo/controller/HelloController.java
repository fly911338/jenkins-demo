package com.kevin.jenkinsdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/hello")
public class HelloController {

    @GetMapping("/print")
    public String printHello() {
        String hello = "hello :((";
        System.out.println(hello);
        return hello;
    }
}
